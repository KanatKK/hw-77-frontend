import React from 'react';

const Send = props => {
    const disabled = props.message === "";
    return (
        <div className="send">
            <input type="text" value={props.author} className="author" placeholder="Author" onChange={props.addAuthor}/>
            <input type="text" value={props.message} className="message" placeholder="Message" onChange={props.addText}/>
            <label className="inputFileLabel" htmlFor="inputFile">{props.imageName}</label>
            <input type="file" className="image" id="inputFile" accept=".jpg, .jpeg, .png" onChange={props.addImage}/>
            <button type="button" disabled={disabled} className="sendBtn" onClick={props.send}>Send</button>
        </div>
    );
};

export default Send;