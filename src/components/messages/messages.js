import React from 'react';

const Messages = props => {
    return (
        <div className="messages">
            {props.image && <img className="avatar" src={'http://localhost:8000/uploads/' + props.image} alt="avatar"/>}
            <h3>{props.author}</h3>
            <p>{props.message}</p>
            <p className="dateTime">{props.dateTime}</p>
        </div>
    );
};

export default Messages;