export const GET_MESSAGES = "GET_MESSAGES";

export const ADD_TEXT = "ADD_TEXT";
export const ADD_AUTHOR = "ADD_AUTHOR";
export const ADD_IMAGE = "ADD_IMAGE";