import {GET_MESSAGES} from "../actionTypes";

const initialState = {
    messages: null,
};

const getMessages = (state= initialState, action) => {
    switch (action.type) {
        case GET_MESSAGES:
            return {...state, messages: action.value};
        default:
            return state;
    }
};

export default getMessages;