import {ADD_AUTHOR, ADD_IMAGE, ADD_TEXT} from "../actionTypes";

const initialState = {
    message: '',
    author: '',
    image: null,
    imageName: 'Choose an image...',
};

const postMessages = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TEXT:
            return {...state, message: action.value};
        case ADD_AUTHOR:
            return {...state, author: action.value};
        case ADD_IMAGE:
            return {...state, image: action.value, imageName: action.value.name};
        default:
            return state;
    }
};

export default postMessages;