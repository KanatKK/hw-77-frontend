import React, {useEffect} from 'react';
import './App.css';
import Messages from "../../components/messages/messages";
import Send from "../../components/send/send";
import axios from "axios";
import {useDispatch, useSelector} from "react-redux";
import {addAuthor, addImage, addNewMessage, addText, fetchMessages} from "../../store/actions";

function App() {
    const dispatch = useDispatch();
    const messages = useSelector(state => state.get.messages);
    const text = useSelector(state => state.post.message);
    const author = useSelector(state => state.post.author);
    const imageName = useSelector(state => state.post.imageName);
    const postReducer = useSelector(state => state.post);

    useEffect(() => {
        dispatch(fetchMessages());
    }, [dispatch]);

    useEffect(() => {
        const interval = setInterval(async() => {
            const response = await axios.get('http://localhost:8000/messages');
            if (response.data.length > 0) {
                dispatch(fetchMessages());
            }
        }, 2000);
        return () => clearInterval(interval);
    }, [messages, dispatch]);

    let addTextHandler = event => {
        dispatch(addText(event.target.value));
    };

    let addAuthorHandler = event => {
        dispatch(addAuthor(event.target.value));
    };

    let addImageHandler = event => {
        if (event.target.files[0] !== undefined) {
            dispatch(addImage(event.target.files[0]));
        } else {
            dispatch(addImage({name: "Choose an image..."}));
        }
    };

    let sendMessage = async () => {
        const formData = new FormData();
        await Object.keys(postReducer).forEach(key => {
            formData.append(key, postReducer[key]);
        });
        await addNewMessage(formData);
        dispatch(addText(""));
        dispatch(addAuthor(""));
        dispatch(addImage({name: "Choose an image..."}));
    };

    if (messages) {
        let messagesList = messages.map((messages, index) => {
            return (
                <Messages
                    key={index} author={messages.author} image={messages.image}
                    message={messages.message} dateTime={messages.datetime}
                />
            );
        });
        return (
            <div className="container">
                <div className="allMessages">
                    {messagesList}
                </div>
                <Send
                    message={text} author={author} imageName={imageName}
                    addText={addTextHandler} addAuthor={addAuthorHandler}
                    addImage={addImageHandler} send={sendMessage}
                />
            </div>
        );
    } else {
        return (
            <div className="container">
                <Send
                    message={text} author={author} imageName={imageName}
                    addText={addTextHandler} addAuthor={addAuthorHandler}
                    addImage={addImageHandler} send={sendMessage}
                />
            </div>
        )
    }
}

export default App;